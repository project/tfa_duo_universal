<?php

namespace Drupal\tfa_duo_universal\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * TFA Redirect.
 */
class DuoController extends ControllerBase {

  /**
   *
   */
  public function __construct() {
    $plugin_settings = \Drupal::config('tfa.settings')->get('validation_plugin_settings');
    $this->settings = !empty($plugin_settings['tfa_duo_universal']) ? $plugin_settings['tfa_duo_universal'] : [];
    $this->keyRepository = \Drupal::service('key.repository');
  }

  /**
   * {@inheritdoc}
   */
  public function duologin(Request $request) {
    $values = $this->keyRepository->getKey($this->settings['duo_key'])->getKeyValues();
    $redirect_url = !empty($values['duo_redirecturl']) ? $values['duo_redirecturl'] : '/';
    if (\Drupal::currentUser()->isAnonymous()) {
      $tempstore = \Drupal::service('tempstore.private')->get('tfa_universal_duo');
      $temp_data = $tempstore->get('state_values');
      $state = $request->get('state');
      if (!empty($temp_data['username']) && isset($temp_data['state']) && !empty($temp_data['state']) && $temp_data['state'] == $state) {
        $user = user_load_by_name($temp_data['username']);
        user_login_finalize($user);
        $this->messenger()->addMessage('Login Successfull.');
        return new RedirectResponse($redirect_url);
      }
      else {

        $this->messenger()->addError('TFA Not Successfull. Please Login Again.');
        return new RedirectResponse('/');
      }
    }
    else {
      return new RedirectResponse('/');
    }
  }

}
