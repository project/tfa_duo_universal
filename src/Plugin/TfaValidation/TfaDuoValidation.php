<?php

namespace Drupal\tfa_duo_universal\Plugin\TfaValidation;

use Drupal\user\Entity\User;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaBasePlugin;
use Drupal\tfa\Plugin\TfaValidationInterface;
use Drupal\tfa\TfaLoginTrait;
use Drupal\user\UserDataInterface;
use Duo\DuoUniversal\Client;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Duo validation class for performing Duo validation.
 *
 * @TfaValidation(
 *   id = "tfa_duo_universal",
 *   label = @Translation("Tfa Duo validation"),
 *   description = @Translation("Tfa Duo Validation Plugin"),
 *   isFallback = FALSE
 * )
 */
class TfaDuoValidation extends TfaBasePlugin implements TfaValidationInterface {
  use MessengerTrait;
  use StringTranslationTrait;
  use TfaLoginTrait;

  /**
   * Object containing the external validation library.
   *
   * @var \Duo\Web
   */
  protected $duo;

  /**
   * This plugin's settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * The KeyRepository.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service);
    $plugin_settings = \Drupal::config('tfa.settings')->get('validation_plugin_settings');
    $this->settings = !empty($plugin_settings['tfa_duo_universal']) ? $plugin_settings['tfa_duo_universal'] : [];
    $this->keyRepository = \Drupal::service('key.repository');
    $values = $this->keyRepository->getKey($this->settings['duo_key'])->getKeyValues();
    $this->duo = new Client($values['duo_clientid'], $values['duo_clientsecret'], $values['duo_apihostname'], $values['baseurl'] . '/duo-callback');
  }

  /**
   * {@inheritdoc}
   */
  public function ready() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state) {
    $user = User::load($this->uid);
    $username = $user->getDisplayName();
    // Return the DUO iFrame form to present TFA options.
    return $this->getTfaDuoUrl($username);
  }

  /**
   * Returns the DUO TFA URL.
   *
   * @param string $username
   *   The username of currently logged user.
   */
  protected function getTfaDuoUrl($username) {

    // we'll build the Duo url.
    $state = $this->duo->generateState();
    $prompt_uri = $this->duo->createAuthUrl($username, $state);
    if (!empty($prompt_uri)) {
      $temp_data = [
        'state' => $state,
        'username' => $username,
      ];
      $tempstore = \Drupal::service('tempstore.private')->get('tfa_universal_duo');
      $tempstore->set('state_values', $temp_data);
      $redirect = new RedirectResponse($prompt_uri);
      $redirect->send();
    }
    else {
      $redirect = new RedirectResponse('/');
      $redirect->send();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm($config, $state) {
    $form = [];
    $form['duo_key'] = [
      '#type' => 'key_select',
      '#key_filters' => ['type' => 'duo_universal'],
      '#title' => $this->t('Duo key'),
      '#default_value' => !empty($this->settings['duo_key']) ? $this->settings['duo_key'] : '',
      '#description' => $this->t('The Duo key with all the credentials from the Duo administrative interface'),
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, FormStateInterface $form_state) {
    // Invoke the verification method again with the form state values.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbacks() {
    return ($this->pluginDefinition['fallbacks']) ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function isFallback() {
    return ($this->pluginDefinition['isFallback']) ?: FALSE;
  }

}
