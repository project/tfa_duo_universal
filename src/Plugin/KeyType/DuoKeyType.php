<?php

namespace Drupal\tfa_duo_universal\Plugin\KeyType;

use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key that combines the crendentials to authenticate to Duo.com.
 *
 * @KeyType(
 *   id = "duo_universal",
 *   label = @Translation("Duo Universal"),
 *   description = @Translation("A key type to store credentials to authenticate to Duo.com."),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "textarea_field"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "duo_clientid" = @Translation("Client ID"),
 *       "duo_clientsecret" = @Translation("Client Secret"),
 *       "baseurl" = @Translation("Site Base URL"),
 *       "duo_redirecturl" = @Translation("Redirect Url"),
 *       "duo_apihostname" = @Translation("API hostname")
 *     }
 *   }
 * )
 */
class DuoKeyType extends AuthenticationMultivalueKeyType {
}
