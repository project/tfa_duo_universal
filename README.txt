CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Two Factor Authentication for Duo Security Universal module provides a plugin to the
tfa framework for Duo Security.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tfa_duo_universal

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/tfa_duo_universal


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Key - https://www.drupal.org/project/key
 * TFA - https://www.drupal.org/project/tfa


INSTALLATION
------------

 * Install the Two Factor Authentication for Duo Security Universal module as you would
   normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
Things you will need:
- An encryption profile using a valid encryption key.
- A key of type Duo with your credentials in json format:
  {"duo_clientid":"ClientId of Application","duo_clientsecret":"ClientSecret of Application","baseurl":"BaseUrl of Website","duo_redirecturl":"Something","duo_apihostname":"SOMETHING"}
* "duo_clientid" - A key YOU generate and keep secret from Duo. It should be at least 40 characters long.
* "duo_clientsecret" - The "Clienr Secret" in the Duo web site
* "baseurl" - The BaseUrl in the  web site
* "duo_redirecturl" - The redirect URL after successfull TFA
* "duo_apihostname" - The "API hostname" in the Duo web site

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > People > Two-factor
       Authentication to enable this plugin.


MAINTAINERS
-----------

 * Mahesh Sankhala - https://www.drupal.org/u/msankhala
 * Surya Kumar - https://www.drupal.org/u/suryakumar1626
